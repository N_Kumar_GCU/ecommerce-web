-- MySQL dump 10.13  Distrib 8.0.21, for macos10.15 (x86_64)
--
-- Host: localhost    Database: Wearhouse
-- ------------------------------------------------------
-- Server version	5.7.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Users`
--

DROP TABLE IF EXISTS `Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Users` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `name` tinytext NOT NULL,
  `username` tinytext NOT NULL,
  `passkey` longtext NOT NULL,
  `Role` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Role` (`Role`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`Role`) REFERENCES `userRoles` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Users`
--

LOCK TABLES `Users` WRITE;
/*!40000 ALTER TABLE `Users` DISABLE KEYS */;
INSERT INTO `Users` VALUES (1,'Nathaniel Kumar','test','$2y$10$sh1TtWQrar9/p/ihKP7W..Cd5u15IwbAiWjp2Q3SMyAu.QYp6.xYi',0),(2,'Rafael Blackburn','FaceDown30','$2y$10$WoBqn4SN5k8wSMY9ioHNp.awl9w4EzyqH9r79Ykx.n7ljndJhoCVe',0),(3,'Jerome Stark','Mill100','$2y$10$i4xdSCW2wTR273/XwLDkQO5SVEHysaLGhdy0LuEX96MVkoIFHow/e',0),(4,'Levi Sexton','ProtractR21','$2y$10$EYrNBSoK.QwV1r.2JnQO5edBkOpfD.oq9A2MICQi3/yFhrR6mqTMe',0),(5,'Chaim Mays','Platform30','$2y$10$69nXVf0aLsNBa3XL9/C6oOiCUG0STbgRr8heOVs/SShjWdMPU6W8.',0),(10,'Cannon Christian','Sculptor91','$2y$10$Bsm0qqf3YxS2GJpfkgPo5uh1840y2DJX9jChJf7x2r676kQNAPpWW',0),(11,'Xander Knight','MrKeyboard20','$2y$10$jPdB8hY4msKeuVvauQ44J.Zg1mgQm6jud4qsNIKOqHUl7h2gVvsIy',0),(12,'Spencer Petty','Director87','$2y$10$jP86xhNRv1fFd8FQVGc2Uu.J7Z8V8AeM4GHLjCuUqkXVWBzdrYV7W',0),(13,'Trevin Cordova','D1SCov3r79','$2y$10$gz3pHU9VD1ZNERu.10Hkv.Tldmer9Ua94NQO1MsYfNUbBcosaHW0O',0),(14,'Mitchell Boyd','shooting32','$2y$10$H8grupCG1HuEC4/LWM4wwedVUNQVnxhOdDcH92tHBvmOIGM9ymb4.',0),(15,'Ramiro Davenport','hermione10','$2y$10$a/DZ4XnqsiBTht9C1OFOhujadhUvhJek29gSGaNT9d9sfbP2KFB9O',0),(16,'Billy Wu','kirkwood64','$2y$10$fZkuqMJ.tK7P1bVEu0Gyqu0oMRfktFtgXwbg.PkwVTOfkROV/J.z2',0),(17,'Calvin Ali','increase34','$2y$10$z6.3T6FYpk.YNnQ4c.htFeptUNaII6qqXhjCgqA5cv/6LylJSk1Cy',0),(18,'Aiden Cook','0verturn74','$2y$10$i4MspvlhggygLOO1koUJguq.D7BpGe7owNTdOa.iwpndz574DvRoC',0),(19,'Brooks Mccarthy','Piquillo64','$2y$10$Y1bmycI4oBCr6.Y5mdiC9O1z6xKtJ.GXK3Hi4qxSJWMpWTp4GHWCS',0),(20,'Jace Bryant','Catfight67','$2y$10$QalUiAB.deXeMP9lnkH.sessEMaks2bSfRghBY2lCP1gmywyVgiN6',0),(21,'Zachariah Dennis','Nightjar96','$2y$10$XW1QlHfsMGEd6MLUyIbk7./KYiF5C1CR3Gn/xnqFk8eqCSYcEV.76',0),(22,'Felipe Mosley','decrease06','$2y$10$09.3TPXPEnG5/v24huO2EuSBJdmMpdTNPU7RdteIYndZoxZj05bIi',0),(23,'Rayan Knox','beaulieu87','$2y$10$Fh3hF7eN3M4I7bYTe1vDPOp1aZt3N3UsCuwfBcT467G2wklz6Y4Oq',0),(24,'Darrell Hurst','obstruct96','$2y$10$K7xhNCoS7OR.YdxON8zly.zl0ldtjZ3XGEn.G8z9ikgbXQJSr7CZK',0),(25,'Ronan Walter','gruesome82','$2y$10$PWWY0mwPT062X/lWly052ekilgO9oB8tYMzGWx0xWGLzYtriep.TO',0),(26,'Jair Moran','smallpox57','$2y$10$JTGXFEzoV74TVqALzMWy9evKfzhdVKToqtREutzk0yGFwpxFPtq0C',0),(27,'Terrence Gay','severity83','$2y$10$gw7L1.GgZaWrWOZtgSWz.uHpoLCc6NMUU5MtVJizmzDaKHY8DQvyO',0),(28,'Branden Todd','skeleton24','$2y$10$QUkk4sVTTCTkecJzR7rDI.obuut6kPRUPyjOjvl841FYm7c8QiIwW',0),(29,'Anton Garner','Stencils42','$2y$10$RGg.PAjtHHWMU/1cHCUBkObp8ivqcEoic9Si1eZBxoxn6HVNf71KW',0),(30,'Kadin Rice','humorist49','$2y$10$qidUAa6bl40UaGrIVV1eQuwBN33MHjx945DSbluXDgMSubnl1GpnG',0),(31,'Bennett Vincent','nuptials6','$2y$10$r0OeqYkOnzW7W3xbaRtwo.cTLXchYRoMbAJQ7J6bUkInDRNP4U556',0),(32,'Deegan Flynn','pretense79','$2y$10$dCYpCD8A5HG5NiSR0o3hXuRCWDBa5ORxE1OeAqFSbGi5WMN3JLMCa',0),(33,'Keshawn Mccarty','tattling2','$2y$10$Z3d7Qjjz6IH9MC.nut76BuadKnauOfLFNrxaH7nj11qTybNecZ2T2',0),(34,'Emiliano Barrett','invented40','$2y$10$v7GR9gj.CO4yp2Sj75pJAOkJxaWfcrJL0USE0nY0.u6hQ0eZ6rMpW',0),(35,'Walter Gallagher','recycler29','$2y$10$Pez/epz/uNMO3UD7pl2hWulej42JHiZNFjiomlPuwL1.fAscD7gzW',0),(36,'Conner Calhoun','employee7','$2y$10$F6bkFNoB1UPdqBgqbKW0h.hSnKB4KnlVvmGARYQAnMuhKtsAzFJ2i',0),(37,'Omar Good','catalyze62','$2y$10$qV98vKS.8HKC3eb0dsxHVeWh64WkLcibVpAML..R4wahTUJecUoPa',0),(38,'Ahmad Rosario','moderato32','$2y$10$VZHMmjTb2cQQWLlxIZH4KuW8pliyXF1V6cM6CC3kQ62Vf7.01AlYq',0),(39,'Urijah Dorsey','friesian26','$2y$10$aTXmN/iR4lQLvfjPbZxrmuFR8ccLjLgqerQ8h/lnHag452iJtKfsu',0),(40,'Jovan Andrews','Romanian61','$2y$10$2/LtHKZfBmFOKGosYLwtzeuJrQLzULLTwLCsDbmnGsoyyCzjasPOq',0),(41,'Gary Anthony','heritage22','$2y$10$pfb7Z1Tua2wWaUVEa9M9h.vj/cw7RMMw/JmSzEFn1eOwrzi/kvs4q',0),(42,'Jaydin Porter','brownlow23','$2y$10$H7zncPLKsadpMoX.L7ZfwODY6Y3ZjmW1TkYRli5/g7ORB.H.mwYui',0),(43,'Nehemiah Lawson','chemical49','$2y$10$oEUUL2BPaOXAG52.ztkeB.bSQkE46CemrYTMZfNlfEyb47yuEToyq',0),(44,'Frederick Ball','palpable90','$2y$10$xBOJNc.L3qgDjspow9Msb.kvZKWpX4gc9V7F8PN/1hcB/z6bii0rG',0),(45,'Jean Hanna','madeline8','$2y$10$14fgp4TogwKQoRiJSP05EuCmcVlwIkbj3Y2Ql6QmkjjdY0oWdXY/6',0),(46,'Brennan Page','champion14','$2y$10$QmgiGJIAxXq.MAJMUbqEremlHy.olWv1zc47ANGaPgFdZc4ld7ij6',0),(47,'Brock Faulkner','cruckles72','$2y$10$oE5XxE4p70mKQbBXYtVVnuPBaXR92ibmhJ.OXcXAFRLyPYmoSOYhi',0),(48,'Harper Matthews','gatorade44','$2y$10$XtH3MUKxZfvX.yuxRQZyE.1jcDI66gUzCmxFFZ76gPcU16RUfvNzO',0),(49,'Tyree Hahn','harmless68','$2y$10$/gmyTUWVwHKCcQ3UfGyMUONPfDM8LH3zbotkNUBf0v16VLRSjWI86',0),(50,'Aden Hensley','pussface6','$2y$10$oJflCfPxCMv1t.n5vdtmSOYUg5d4sMq42VDtiwp4.zGAMaTBPn5oO',0),(51,'Fabian Gutierrez','reactant95','$2y$10$4lEKB23mj5zu9ulmfpHS4eoKqI2Ny2lf0YVsS04u1.PDq5wFldwMG',0),(52,'Tyrell Hendrix','biscuits73','$2y$10$FKYg.flexTS6y1/BNO60UOq10H59CfKHQFeShAJsUE5NMcdM.sZz6',0),(53,'Kason Duke','upheaval28','$2y$10$HKttZJmkWbF7FPDqIJ5H2.7YYkYFaFk41mlmYTnni813C0egbjdTS',0),(54,'Easton Velazquez','finching97','$2y$10$HWQUjO2IrYITE5I7q651R.HZwjmBEA8wJJSkgobhwdyg01TuOtDOy',0),(55,'Braedon Logan','pendulum92','$2y$10$.ojEaxMfh6MUf0tQiJBSVuLVDAdjvuGEoYEvuFarILLAOtHZdh13a',0),(56,'Hezekiah Macintosh','distrust27','$2y$10$nFoXhJf4j6cTpe6uSuEaVO4swgUhwQIeLm7nykXwqt38K80nfL0ze',0),(57,'Nikolai Ayers','marigold88','$2y$10$2kzR2ONv2y82KRS4gPObbuy5WydlB.M2FSeTpvGJ7rK6oR4Qk42.2',0),(58,'Ray Blackwell','maryland89','$2y$10$sd/2RLs47Fzu0/zkCoNDSOVCDrz9X0gcnh0PohdXFfvACJ7ql2E6C',0),(59,'Finley Cochran','glaucoma15','$2y$10$K6ER4NjShVADLghPN2xyP.bhrLtjVQDhQ3VSjjrw.hggcGBLU6fMG',0),(60,'Valentin Stephens','mackerel55','$2y$10$NRfonG8b1m10JOyNz7zqYufOIA6E9yrlNg//./TLQV0qP/g8NDXvy',0),(61,'Ali Wade','cruncher74','$2y$10$4rGGUXUKL/ziXqrFwTtxb.sYnVpD9CUyY5SACPgWetgFpVotlaw3S',0),(62,'Asher Gonzales','pointing80','$2y$10$//ArYAC1o8kQcdLQF/gSm.wUn/rtfBY5OWPcrA8NFk/TjbbG1Dzy2',0),(63,'Jamal Carney','negative38','$2y$10$Bl/tx4AdVN5r9xzSmPfLU.GQgmJ0OW9oYJfgkWSVT/8TW49C1idAi',0),(64,'Eugene Stokes','gyration10','$2y$10$DedXWjDPfx8RMz6JoHjGH.RQdxJ4FMZbPd3/xvgBmjerxTwvViRYC',0),(65,'Khalil Caldwell','termites34','$2y$10$6mAKINce3imQyt.eIQDe3u7TJksOF3cE8/UPCZb0A/xmGUJiC1iDO',0),(67,'Chase Chapman','scornful61','$2y$10$gCRiHLr95dWB5yoydgBASOBIxXv2k/2Yq4cdS.yH8vAjdFYTZVxU2',0),(68,'Adam Smith','AppleSmith','$2y$10$/TUUV9h4IXCmd09jsvaRTOPnBvskp674tyJD3ISDRg2RF9zQ3Saru',0),(69,'AMW Admin','ApostolicAdmin77','$2y$10$egoxmkXas7DFV73KKzB6ueCA/oAsgQN94i3P.pkxxESqG7Frwz9N2',1),(73,'Admin','Admin','$2y$10$6S7uVor/APTAnU5tlKct.OZr9u5hiUZDBRoMgIE9ewz8lp/OYvSXy',1);
/*!40000 ALTER TABLE `Users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-24 20:39:59
